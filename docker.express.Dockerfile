FROM node:8 AS builder
WORKDIR /usr/src/app
COPY package.json .
RUN npm install
COPY . .
RUN npm run build

FROM node:8 as runtime
WORKDIR /usr/src/app
COPY servers/express/package.json .
RUN npm install
COPY servers/express/app.js .
COPY --from=builder /usr/src/app/build build
EXPOSE 3000
CMD ["npm", "start"]
