FROM node:8

WORKDIR /usr/src/app

# ENV PATH /usr/src/app/node_modules/.bin:$PATH

COPY package.json .

RUN npm install
# RUN npm install react-scripts@2.1.1 -g

COPY . .

RUN npm run build

CMD ["npm", "start"]
