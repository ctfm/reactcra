const express = require('express');
const path = require('path');
const app = express();
const port = 3000;

app.use(express.static(path.join(__dirname, 'build')));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/hello', (req, res) => res.send('hello page (/hello)'));

app.get('/hellofile', (req, res) => res.sendfile(path.join(__dirname, 'public', 'hello.html')));

app.get('/*', (req, res) => { 
	res.sendFile(path.join(__dirname, 'build', 'index.html'))
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
